const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    home: path.resolve(__dirname, 'src/js/index.js'),
    precios: path.resolve(__dirname, 'src/js/precios.js'),
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename:'js/[name].js'
  },
  module:{
    rules:[
      // Aqui tendremos nuestros loaders, que son los que nos permiten soportar multiples tipos de archivo, dentro de nuestro archivo de javascript, este recibe 2 parametros(test,use)
      {
        // test: que tipo de archivo quiero reconocer
        // use: cual loader se va a encargar del archivo -- en este caso instalaremos 2(style-loader, css-loader)
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use:'css-loader'
        })
      }
    ]
  },
  plugins:[
    // aquí van los plugins
    new ExtractTextPlugin('css/[name].css')
  ]

}
