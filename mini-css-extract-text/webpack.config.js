const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    principal: path.resolve(__dirname, 'index.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename:'bundle.js'
  },
  module:{
    rules:[
      // Aqui tendremos nuestros loaders, que son los que nos permiten soportar multiples tipos de archivo, dentro de nuestro archivo de javascript, este recibe 2 parametros(test,use)
      {
        // test: que tipo de archivo quiero reconocer
        // use: cual loader se va a encargar del archivo -- en este caso instalaremos 2(style-loader, css-loader)
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      }
    ]
  },
  plugins:[
    // aquí van los plugins
    new MiniCssExtractPlugin({
        filename: "./css/[name].css"
      })
  ]

}
