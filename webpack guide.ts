// instalar webpack así:
npm install webpack --save-dev

// Ya salió la versión 4 de webpack, y el Command Line Interface (CLI) ahora viene por separado, hay que instalar de la siguiente manera:
npm i webpack webpack-cli --save-dev

// o si prefieren usar yarn:

yarn add webpack webpack-cli --dev


// Configurando nuestro primer bundle con un entry point, al tener webpack instalado localmente debemos agregar el script en packaje.json así:
"build": "webpack index.js --output bundle.js --mode development"
      // --mode development es para quitar el anuncio de warning que sale en consola

//Debemos crear el archivo "webpack.config.js" y posteriormente llamarlo con un script mas sencillo que por defecto llama a este archivo"webpack.config.js" de la siguiente forma1
"build": "webpack"

//para el manejo de rutas relativas podemos usar el siguiente script con el flag --config que nos permite añadir rutas para buscar archivos especificos, en este caso lo que tenemos dentro de la carpeta external
"build:external": "webpack --config ./external/webpack.config.js"


// para este proyecto instalamos y utilizamos los 2 siguientes loaders
npm install style-loader css-loader --save-dev
// Gracias css loader yo voy a poder cargar archivos de css e interpretarlos dentro de javascript, en el archivo de compilación final bundle.js
{
  test: /\.css$/,
  use: ['style-loader','css-loader']
}

// >>EXTRACT-TEXT<<
 // PLUGIN UTILIZADO PARA EXPORTAR LOS ARCHIVOS CSS PARA EL ENTORNO DE PRODUCCIÓN, ver forma de utilización en archivo webpack ya creado
 // para instalar extract-text-webpack-plugin en "webpack 4 o superior" debe de hacerlo con el siguiente comando para que funcione:
 npm i -D extract-text-webpack-plugin@next
 // se recomienda utilizar para la ultima versión la siguiente dependencia en vez de extract-text-webpack:
 npm install --save-dev mini-css-extract-plugin

 // agregando "--watch" en el script, webpack nos permite actualizar los archivos del build solo actualizando nuestro navegador

"--watch"

"build:multi": "webpack --config ./multiples/webpack.config.js --mode development --watch",

 //Con esta nueva dependencia: "webpack-dev-server" podemos tambien montar un pequeño servidor que actualizara y recompilará todo el build en automatico como lo hace por ejemplo angular4


 npm i webpack-dev-server --save-dev

   {
     --content-base <file/directory/url/port>
     // — define la ruta base para los contenidos
     --quiet
     // — no mostrar salidas a la consola
     --no-info
     // — omitir la información innecesaria
     --colors
     // — añadir color a los mensajes de salida
     --no-colors
     // — no usar colores en los mensajes de salida
     --host <hostname/ip>
     // — define nombre o IP del host
     --port <number>
     // — define qué puerto usar
     --inline
     // – incorpora la webpack-dev-server runtime en el paquete
     --hot
     // — añadir el HotModuleReplacementPlugin y pasar a modo en caliente (hot).
     // — NOTA: se debe evitar la habilitación doble: por parametros en el webpack.config.js + vía CLI.
     --hot --inline
     // — similar a webpack/hot/dev-server
     --lazy
     // — no habilita la observación (watch), debe evitarse el uso con --hot.
     --https
     // — inicia el servidor webpack-dev-server sobre el protocolo HTTPS.
     // — Incluye un certificado digital auto-firmado para atender los requests.
     --cert, --cacert, --key
     // — indicar las rutas de ubicación de los archivos del certificado

     "Opciones que aplican al archivo webpack.config.js:"

     noInfo
     // — Omite mostrar información innecesaria en la consola
     // — Default: false
     quiet
     // — No muestra nada en la consola
     // — Default: false
     lazy
     // — Cambia al modo lazy
     // —Default: false
     filename
     // — en modo lazy: el cambio de peticiones dispara la recompilación
     // — En la mayoría de los casos funciona igual que la configuración output.filename de js.
     watchOptions.aggregateTimeout
     // — Retrasa la recompilacion luego de los cambios. El valor es en ms.
     // —Default: 300
     watchOptions.poll
     // — true: usa chequeo
     // — numero: usa el chequeo en intervalos
     // —Default: undefined
     publicPath
     // — la ruta para enlazar el middleware con el servidor.
     // — En la mayoría de los casos es la misma que la configuracion output.publicPath del js.
     headers
     // — añadir cabeceras personalizadas. i. e. { “X-Custom-Header”: “yes” }
     stats
     // —Muestra en la salida opciones estadísticas.
   }
   // Documentación webpack-dev-server


// SOPORTE ECMASCRIPT con BABEL documentación completa en : https://github.com/babel/babel-loader procedemos a instalar con el comando.

npm install "babel-loader@^8.0.0-beta" @babel/core @babel/preset-env -D

// Esto nos instalara babel-loader en su version 8 que nos soportara babel 7, luego nos instalara babel en su version 7 y por ultimo instalara el preset-env que sera el que utilizaremos.
// segundo:
// en el archivo webpack.config.js cargaremos el babel-loader sin pasarle ninguna configuracion de esta manera:

{ test: /\.js$/, use:{loader: 'babel-loader'}}

// Tercero:  Instalaremos los siguiente :

npm install --save-dev @babel/plugin-transform-runtime
// y
npm install --save @babel/runtime

//
<<.babelrc>>
// Cuarto: Crearemos un archivo dentro del mismo directorio donde esta webpack.config.js llamado .babelrc, este es el archivo de configuración de babel dentro de el colocaremos lo siguiente:

{
    "presets": [
      ["@babel/preset-env"],
      // ["@babel/preset-react"]
      // Se agrega el preset para trabajar con react y sus archivos .jsx ver abajo la instalación de las dependencias React.js
    ],
    "plugins": ["@babel/plugin-transform-runtime"]
}

// si no se utiliza el archivo .babelrc se puede hacer desde el loader de babel en el archivo webpack.config.js de la siguiente forma
{
      test: /\.js$/,
      use: {
          loader: 'babel-loader',
          options: {
              presets: ['@babel/preset-env','@babel/preset-react']
          }
      },
  }


// La forma correcta de utilizar el loader en el webpack es:
          {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['babel-preset-env']
                    }
                },
            }

// << ReactJs >>
// Instalamos las siguientes dependencias SOPORTE EN: https://new.babeljs.io/docs/en/next/babel-preset-react.html

npm i -D @babel/preset-react
npm i -S react react-dom

// react y react-dom … a diferencia de las dependencias usadas hasta el momento en el curso, se instala sólo con –save o -S para indicar a npm que al momento de generar el paquete final deberá incluirlas como parte de los archivos de distribución. Ya que el código de nuestro programa las usará durante la ejecución en producción.
// Las dependencias de desarrollo, permiten transpilar código, pre-procesar, interpretar, etc. que son tareas en tiempo de desarrollo … y una vez que se ejecuta npm run build:guarever … y se generan los archivos de distribución, ya no son más necesarias para la ejecución del programa.
