import React from 'react';

function Teacher(props){
  return(
    <li className="Teacher">
      {props.name}<a href={`https://twitter.com/${props.twiter}`}>{props.twiter}</a>
    </li>
  )
}

export default Teacher;
