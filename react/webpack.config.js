const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  entry: {
    principal: path.resolve(__dirname, 'src/js/index.js')
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename:'bundle.js'
  },
  devServer: {
    port: 9000,
  },
  module:{
    rules:[
      // {
      //   test: /\.css$/,
      //   use: [
      //     MiniCssExtractPlugin.loader,
      //     'css-loader'
      //   ]
      // },
      {
        test: /\.css$/,
        use: ['style-loader','css-loader']
      },
      {
            test: /\.js$/,
            use: {
                loader: 'babel-loader',
                // options: {
                //     presets: ['@babel/preset-env','@babel/preset-react']
                // }
                // solo se habilita las lineas comentadas de arriba si no se utiliza el archivo .babelrc con estas mismas especificaciones
            },
        }
    ]
  },
  plugins:[
    new MiniCssExtractPlugin({
        filename: "./css/[name].css"
      })
  ]

}
