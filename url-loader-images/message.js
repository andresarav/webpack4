import rendertoDom from './render-to-dom';
import makeMessage from './make-message';

const waitTime = new Promise((todoOk, todoMal)=>{
  setTimeout(() => {
    todoOk('Han pasado 3 segundos')
  }, 3000)
})

module.exports = {
  firstMessage: 'Hola cabrón, este es el mensaje que estas importando querido',
  delayMessage: async () =>{
     const message = await waitTime;
     console.log(message);
     // const element = document.createElement('p');
     // element.textContent = message;
     rendertoDom(makeMessage(message));
  }
}
